import struct
import logging

import tornado.web

import lamp_control

HTML_PAGE_ON = """
<html>
    <head>
        <title>WEBLAMP</title>
        <style>
            body {background-color: %s;}
            p {color: %s; font-size:144;}
        </style>
    </head>
    <body>
        <table width=100%% height=100%%>
            <tr><td align=center>
                <p> %s </p>
            </td></tr>
        </table>
    </body>
</html>
"""

HTML_PAGE_OFF = """
<html>
    <head>
        <title>WEBLAMP /OFF/</title>
        <style>
            body {background-color: black;}
            p {color: white; font-size:144;}
        </style>
    </head>
    <body>
        <table width=100% height=100%>
            <tr><td align=center>
                <p> OFF </p>
            </td></tr>
        </table>
    </body>
</html>
"""


class WebLampController(lamp_control.BaseLampController):
    class WebHandler(tornado.web.RequestHandler):
        def get(self):
            self.write(self._get_page())

    def __init__(self, port):
        self.on = False
        self.color = '#ffffff'
        self.revcolor = '#000000'
        self.port = port
        self.__class__.WebHandler._get_page = self._get_page
        app = tornado.web.Application([
            (r"/", self.__class__.WebHandler),
        ])
        app.listen(self.port)
        logging.info("Web lamp viz active on local port %i", self.port)

    def _get_page(self):
        if self.on:
            return HTML_PAGE_ON % (self.color, self.revcolor, self.color)
        else:
            return HTML_PAGE_OFF

    def _control_ON(self, value):
        self.on = True

    def _control_OFF(self, value):
        self.on = False

    def _control_COLOR(self, value):
        rgb = struct.unpack('!BBB', value)
        self.color = '#{:02x}{:02x}{:02x}'.format(*rgb)
        self.revcolor = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: 0xff - x, rgb))

