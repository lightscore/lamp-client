#!/usr/bin/env python

import socket
import struct
import functools
import logging
import threading
import time
import signal
import argparse

import tornado.iostream
import tornado.ioloop

import lamp_control
import web_lamp_control


class Lamp(object):
    '''
        Main class for lamp client.
        Handles connection to server and invocation of lamp controls.
        Controls itself should be accessible as the methods of controller object.
    '''
    def __init__(self, controller, host, port):
        self.controller = controller
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.stream = tornado.iostream.IOStream(s)
        self.stream.connect((host, port), self._handle)
        self.ioloop = tornado.ioloop.IOLoop.instance()
        self.ioloop_thread = threading.Thread(target=self._ioloop_thread_target)
        self.ioloop_thread.start()

    def _ioloop_thread_target(self):
        self.ioloop.start()

    def stop(self):
        self.ioloop.stop()
        self.stream.close()

    def _handle(self):
        self.stream.read_bytes(3, self._handle_msg_type_length)

    def _handle_msg_type_length(self, data_TL):
        type_code, length = struct.unpack('!BH', data_TL)
        if type_code in self.controller.msg_types:
            expected_length, type_name = self.controller.msg_types[type_code]
            logging.info("Received message: type %s (%s), declared length %i",
                            hex(type_code), type_name, length)
            if length != expected_length:
                logging.warning("Unexpected length of message value of type %s: %i (expected %i).",
                                type_name, length, expected_length)
            self.stream.read_bytes(length, functools.partial(self._handle_msg_value, type_name))
        else:
            # unknown message type. ignore (almost) silently
            logging.error("Unknown type code %s (length %i). Skipping current message.",
                            hex(type_code), length)
            self.stream.read_bytes(length, self._handle_unknown_type)

    def _handle_msg_value(self, type_name, data_V):
        control_name = '_control_%s' % type_name
        try:
            control = getattr(self.controller, control_name)
        except AttributeError:
            raise NotImplementedError(control_name)
        control(data_V)
        self._handle()

    def _handle_unknown_type(self, value):
        self._handle()


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description="Lamp client")
    arg_parser.add_argument('--lifetime', type=int, help="Client lifetime in seconds", default=60)
    arg_parser.add_argument('--host', type=str, help="Server host", default='127.0.0.1')
    arg_parser.add_argument('--port', type=int, help="Server port", default=9999)
    arg_parser.add_argument('--web-client', action='store_true',
                        help="Web lamp controller (visualizer) instead of console", default=False)
    arg_parser.add_argument('--web-port', type=int, help="Web port", default=19999)
    args = arg_parser.parse_args()

    logging.getLogger().setLevel(logging.INFO)

    if args.web_client:
        lamp_controller = web_lamp_control.WebLampController(port=args.web_port)
    else:
        lamp_controller = lamp_control.ConsoleLampController()
    lamp = Lamp(lamp_controller, args.host, args.port)
    time.sleep(args.lifetime)
    lamp.stop()

