import struct


class BaseLampController(object):
    msg_types = {
        0x12: (0, 'ON'),
        0x13: (0, 'OFF'),
        0x20: (3, 'COLOR'),
    }


class ConsoleLampController(BaseLampController):
    def _control_ON(self, value):
        print "ON"

    def _control_OFF(self, value):
        print "OFF"

    def _control_COLOR(self, value):
        rgb = struct.unpack('!BBB', value)
        str_rgb = '#{:02x}{:02x}{:02x}'.format(*rgb)
        print "COLOR %s" % str_rgb


